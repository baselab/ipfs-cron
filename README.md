
# ipfs-cron

*IPFS update cron job*


## Why

Because `ipfs update` does not work yet.. :(


## Usage

With your ipfs user:

`git clone https://gitlab.com/baselab/ipfs-cron.git`

`crontab -e`

For a 3am weekly update, add a line like this one:

`1 3    * * 7   /path/to/ipfs-cron update 1>/dev/null 2>&1`


## More

You can use this script also for manual operations.

From your really cool terminal emulator:

`bash ipfs-cron`
